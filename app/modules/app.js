'use strict';

/**
 * @ngdoc overview
 * @name templateApp
 * @description
 * # templateApp
 *
 * Main module of the application.
 */
 angular
 .module('templateApp', [
    'modulo1App',
    'ui.router'
  ])
 .config(function ($stateProvider, $urlRouterProvider) {
     // For any unmatched url, redirect to /state1
     $urlRouterProvider.otherwise("/modulo1");
     $stateProvider
     .state('modulo1', {
        templateUrl: 'modules/modulo1/views/modulo1.html',
        controller: 'Modulo1Ctrl',
        url: '/modulo1',
        controllerAs: 'modulo1'
    });
   })
 .controller('MainCtrl', function () {
 });


