'use strict';

angular.module('templateApp')
  .directive('modulo1directive1', function () {
    return {
      template: '<div></div>',
      restrict: 'E',
      link: function postLink(scope, element) {
        element.text('this is the modulo1 directive1');
      }
    };
  });
