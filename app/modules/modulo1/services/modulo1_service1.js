'use strict';

angular.module('templateApp')
.service('modulo1service1', function($http) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    this.exampleoperation = function (parameter1) {
    	return $http({
    		url: "",//Service URL
    		method: "",//POST|GET|PUT|DELETE
    		data: parameter1
    	});
    };
});

