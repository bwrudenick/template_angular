'use strict';

angular.module('templateApp')
  .filter('modulo1filter1', function () {
    return function (input) {
      return 'modulo1 filter1: ' + input;
    };
  });
