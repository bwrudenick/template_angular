'use strict';

/**
 * @ngdoc overview
 * @name templateApp
 * @description
 * # templateApp
 *
 * Main module of the application.
 */
 angular
 .module('modulo1App', [
    'ui.router'
  ])
 .config(function ($stateProvider, $urlRouterProvider) {
     // For any unmatched url, redirect to /state1
     $urlRouterProvider.otherwise("/submodulo1");
     $stateProvider
     .state('modulo1.submodulo1', {
        templateUrl: 'modules/modulo1/sub_modules/modulo1_submodulo1/views/modulo1_submodulo1.html',
        controller: 'Modulo1_submodulo1Ctrl',
        url: '/submodulo1',
        controllerAs: 'modulo1_submodulo1'
    });
   });


